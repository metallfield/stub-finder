<?php


namespace App\Repositories;


use Adaptive\Diff\Diff;
use Adaptive\Diff\Renderer\Text\Unified;
use Adaptive\Diff\Renderer\Text\Context;

class FileRepository
{



    public function finder(string $pattern, $flags = 0) {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->finder($dir.'/'.basename($pattern), $flags));
        }
        return $files;
    }

    public function rootPath($fileName)
    {
        return  substr_replace($fileName, '',0, strlen(base_path()));
    }

    public function twoFileDiff($some_file_path, $some_other_file_path, $options = [])
    {
        $a = explode("\n", file_get_contents($some_file_path));
        $b = explode("\n", file_get_contents($some_other_file_path));

        $diff = new Diff($a, $b, $options);
        $renderer = new Unified();
        return $diff->Render($renderer);
    }

    public function writeFile($stub, $overwriteFile)
    {
        $content = file_get_contents($stub);
        file_put_contents($overwriteFile, $content);
    }
}
