<?php

namespace App\Console\Commands;

use App\Repositories\FileRepository;
use Illuminate\Console\Command;

class StubFinder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stub-finder {dir}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param string $pattern
     * @param int $flags
     * @return array
     */

    const SEARCHING_STUB_PATTERN = '/*.stub';
    public function handle()
    {
        $searchingDirectory = base_path().$this->argument('dir');

        /** @var FileRepository $fileRepository */
        $fileRepository = app(FileRepository::class);

        $stubs = $fileRepository->finder($searchingDirectory.self::SEARCHING_STUB_PATTERN);
        foreach ($stubs as $stub)
        {
            $couple['stub'] = $stub;
            $original = substr($stub, 0, strpos($stub, '.stub'));

            $couple['original'] = $fileRepository->finder($original);
            if (!empty($couple['original']))
            {
                $couple['diff'] = filesize($stub) - filesize($couple['original'][0]);
                if ($couple['diff'] != 0)
                {
                    $action = $this->anticipate('---'.$fileRepository->rootPath($stub). "\n".
                        '+++'.$fileRepository->rootPath($couple['original'][0]). "\n\n".
                        'stub has different size then original:' ."\n".'i - ignore, d - show difference, o - overwrite changes'."\n",
                        [ 'i', 'o','d']);
                    switch ($action){
                        case  'i':
                            continue 2;
                        case 'd':

                            print_r($fileRepository->twoFileDiff($stub, $couple['original'][0] , ['ignoreWhitespace' => true]));
                            do {
                                $secondAction = $this->anticipate('Actions with file:' ."\n".
                                    'i - ignore, o - overwrite changes'."\n",
                                    [ 'i', 'o']);
                                if ($secondAction == 'o')
                                {
                                    $fileRepository->writeFile($stub, $couple['original'][0]);
                                    continue 3;
                                 }
                                if ($secondAction == 'i')
                                {
                                    continue 3;
                                }
                               else{
                                    print_r("\n".'action not exists'. "\n");
                                }
                            }while(true);

                            break;
                        case 'o':
                            $fileRepository->writeFile($stub, $couple['original'][0]);
                            break;
                    }
                }
            }else{
                $fileRepository->writeFile( $stub, $original);
            }
        }
    }
}
